package com.example.rakesh.browse;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.rakesh.browse.utils.Utils;

public class BrowserActivity extends AppCompatActivity {

    private String postUrl = "https://api.androidhive.info/webview/index.html";
    private String url;
    private ProgressBar progressBar;
    WebView webView;
    ImageView imageHeader;
    float m_downX;

    CoordinatorLayout coordinatorLayout;

    android.support.v7.widget.Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        mToolbar = findViewById(R.id.browser_toolbar);
        webView = findViewById(R.id.content_browser_web_view);
        progressBar = findViewById(R.id.progress_bar);
        imageHeader = findViewById(R.id.backdrop);

        coordinatorLayout = findViewById(R.id.content_browser);

        setSupportActionBar(mToolbar);
        mToolbar.setTitle(R.string.app_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        url = getIntent().getStringExtra("url");

        if (TextUtils.isEmpty(url))
            finish();

        initWebView();

        webView.loadUrl(url);
    }

//    public void initWebView() {
//        webView.setWebChromeClient(new MyWebChromeClient(this));
//        webView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//                progressBar.setVisibility(View.VISIBLE);
//            }
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                webView.loadUrl(url);
//                return true;
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                progressBar.setVisibility(View.GONE);
//                invalidateOptionsMenu();
//            }
//
//            @Override
//            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
//                super.onReceivedError(view, request, error);
//                progressBar.setVisibility(View.GONE);
//                invalidateOptionsMenu();
//            }
//        });
//
//        webView.clearCache(true);
//        webView.clearHistory();
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.setHorizontalScrollBarEnabled(false);
//        webView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if (event.getPointerCount() > 0) {
//                    //multi touch detected
//                    return true;
//                }
//
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN : {
//                        m_downX = event.getX(); //saving x
//                        break;
//                    }
//                    case MotionEvent.ACTION_MOVE :
//                    case MotionEvent.ACTION_CANCEL :
//                    case MotionEvent.ACTION_UP: {
//                        event.setLocation(m_downX, event.getY());
//                        break;
//                    }
//
//                }
//                return false;
//            }
//        });
//
//    }

    private void initWebView() {
        webView.setWebChromeClient(new MyWebChromeClient(this));
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
                invalidateOptionsMenu();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                webView.loadUrl(url);
                progressBar.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
                invalidateOptionsMenu();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressBar.setVisibility(View.GONE);
                invalidateOptionsMenu();
            }
        });

        webView.clearCache(true);
        webView.clearHistory();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setSaveEnabled(true);
        webView.getSettings().setSupportMultipleWindows(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setDisplayZoomControls(true);
        webView.setHorizontalScrollBarEnabled(true);

        webView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getPointerCount() > 1) {
                    //Multi touch detected
                    return true;
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        // save the x
                        m_downX = event.getX();
                    }
                    break;

                    case MotionEvent.ACTION_MOVE:
                    case MotionEvent.ACTION_CANCEL:
                    case MotionEvent.ACTION_UP: {
                        // set x so that it doesn't move
                        event.setLocation(m_downX, event.getY());
                    }
                    break;
                }

                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browser_menu, menu);

        if (Utils.isBookmarked(this, webView.getUrl())) {
            // change icon color
            Utils.tintMenuIcon(getApplicationContext(), menu.getItem(0), R.color.colorAccent);
        } else {
            Utils.tintMenuIcon(getApplicationContext(), menu.getItem(0), android.R.color.black);
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        //index 0 is bookmark
        if (webView.canGoBack()) {
            menu.getItem(1).setEnabled(true);
            menu.getItem(1).getIcon().setAlpha(255);
        } else {
            menu.getItem(1).setEnabled(false);
            menu.getItem(1).getIcon().setAlpha(130);
        }
        if (webView.canGoForward()) {
            menu.getItem(2).setEnabled(true);
            menu.getItem(2).getIcon().setAlpha(255);
        } else {
            menu.getItem(2).setEnabled(false);
            menu.getItem(2).getIcon().setAlpha(130);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
            case R.id.action_bookmark : {
                // bookmark / unbookmark the url
                Utils.bookmarkUrl(this, webView.getUrl());

                String msg = Utils.isBookmarked(this, webView.getUrl()) ?
                        webView.getTitle() + "is Bookmarked!" :
                        webView.getTitle() + " removed!";
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
                snackbar.show();

                // refresh the toolbar icons, so that bookmark icon color changes
                // depending on bookmark status
                invalidateOptionsMenu();
            }
            case R.id.action_back: {
                if (webView.canGoBack()) {
                    webView.goBack();
                }
            }
            case R.id.action_foreward: {
                if (webView.canGoForward()) {
                    webView.goForward();
                }
            }

        }

        return super.onOptionsItemSelected(item);
    }

    private class MyWebChromeClient extends WebChromeClient {
        Context context;
        public MyWebChromeClient(Context context) {
            super();
            this.context = context;

        }
    }
}
